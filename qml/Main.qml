import QtQuick 2.9
import QtQuick.Window 2.3
import QtQuick.Controls 2.2
import QtMultimedia 5.12

Window {
    id: window
    width: 800
    height: 480
    title: "Lenses"
    
    property bool selectCameraBoxVisible: false
    
    Item {
        anchors.fill: parent
        
        Camera {
            id: camera
        }

        VideoOutput {
            anchors.fill: parent
            source: camera
            autoOrientation: true
            fillMode: PreserveAspectFit
        }

        Rectangle {
            visible: selectCameraBoxVisible
            anchors {
                right: parent.right
                top: parent.top
                left: parent.left
                margins: 20
            }
            height: cameraList.height
            color: Qt.rgba(0, 0, 0, 0.75)
        
            ListView {
                id: cameraList
                height: contentHeight
                anchors {
                    top: parent.top
                    right: parent.right
                    left: parent.left
                }

                model: QtMultimedia.availableCameras
                delegate: Item {
                    width: parent.width
                    height: cameraLabel.height * 2
                    Text {
                        id: cameraLabel
                        anchors {
                            centerIn: parent
                            margins: 10
                        }
                        width: parent.width
                        text: `${modelData.displayName} (id: ${modelData.deviceId}, position: ${modelData.position}, orientation: ${modelData.orientation})`
                        font.pixelSize: 32
                        color: camera.deviceId === modelData.deviceId ? "#fff" : "#88d"
                        wrapMode: Text.Wrap
                        horizontalAlignment: Text.AlignHCenter

                        MouseArea {
                            anchors.fill: parent
                            onClicked: camera.deviceId = modelData.deviceId
                        }
                    }
                }
            }
        }

        Button {
            anchors {
                right: parent.right
                bottom: parent.bottom
                margins: 10
            }
            text: "Toggle camera list"
            onClicked: selectCameraBoxVisible = !selectCameraBoxVisible
        }
    }
}
