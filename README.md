# Lenses app for Ubuntu Touch

## Building

Just run `clickable build` to build an arch independent package to install on a phone.

## Running

In order to be able to use all the lenses your phone may have you'll need to have a patched "qtubuntu-camera" package installed:

https://gitlab.com/zubozrout/qtubuntu-camera/-/tree/xenial-dont-limit-camera-device


## Debugging

To run and debug this directy on a phone/tablet via USB connection execute:

```
clickable launch && clickable logs
```

Or if you have SSH enabled on your device, you may want to run:

```
clickable --ssh IP_ADDRESSorHOSTNAME && clickable logs --ssh IP_ADDRESSorHOSTNAME
```
